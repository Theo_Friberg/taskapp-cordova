if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function() {
                              FastClick.attach(document.body);
                              }, false);
}

function flip(dest, dir) {
    var options = {
        "direction" : dir,
        "href" : dest
    };
    try{
    window.plugins.nativepagetransitions.flip(options, function (msg) {}, function (msg) {});
    }catch(err){
        document.location = dest
    }
}

function slide(dest, dir) {
    var options = {
        "direction" : dir,
        "href" : dest
    };
    try{
    window.plugins.nativepagetransitions.fade(options, function (msg) {}, function (msg) {document.location = dest});
    }catch(err){
      document.location = dest
    }
}
