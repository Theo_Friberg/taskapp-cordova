
// Copyright (c) 2016 Digisnow Ltd. All rights reserved.

/*

Somehwat generic POST helper function. It posts (via apllication/x-www-form-urlencoded) payload=[payload] to [url]. If the returned code is 200,
it calls callbackSuccess(XMLHttpRequest), else it calls callbackFailure(XMLHttpRequest).

The request is for now SYNCHRONOUS. TODO: Make request ASYNCHRONOUS.

*/

function post(payload, url, callbackSuccess, callbackFailure){
  var http = new XMLHttpRequest()
          var url = url;
          var params = payload;
          http.open("POST", url, false);

          // Send the proper header information along with the request
          http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

          http.onreadystatechange = function() { // Call a function when the state changes.
                      if (http.readyState == 4) {
                          if (http.status == 200) {
                              callbackSuccess(http)
                          } else {
                              callbackFailure(http)
                          }
                      }
                  }
          http.send(params);
}

function request(payload, url, callbackSuccess, callbackFailure) {
    var http = new XMLHttpRequest()
            var url = url;
            var params = "payload="+payload;
            http.open("POST", url, false);

            // Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function() { // Call a function when the state changes.
                        if (http.readyState == 4) {
                            if (http.status == 200) {
                                callbackSuccess(http)
                            } else {
                                callbackFailure(http)
                            }
                        }
                    }
            http.send(params);
}

function getURL(url, callbackSuccess, callbackFailure) {
    var http = new XMLHttpRequest()
            var url = url;
            http.open("GET", url, true);

            // Send the proper header information along with the request
            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            http.onreadystatechange = function() { // Call a function when the state changes.
                        if (http.readyState == 4) {
                            if (http.status == 200) {
                                callbackSuccess(http)
                            } else {
                                callbackFailure(http)
                            }
                        }
                    }
            http.send(null);
}
