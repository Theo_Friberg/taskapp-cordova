
// Copyright (c) 2016 Digisnow Ltd. All rights reserved.

/*

Function to add job.

*/

var imagedata = "";
var perkele = "";

/*function convertFileToDataURLviaFileReader(url) {
    var reader = new FileReader();
    reader.onloadend = function() {
      return reader.result;
    }
    reader.readAsDataURL(url);
    var file = url;//.srcElement.files[0];
    console.log(file);
    var reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onload = function() {
        imagedata = btoa(reader.result);
        send_data();
    };
    reader.onerror = function() {
        console.log('there are some problems');
    };

}*/


function add_job() {
  $("#loader").show();

  if($("#fileupload").val() == ''){
     send_data();
     imagedata = "0";


 }
 else {
   image = preprocess(document.getElementById('fileupload'));

 }
  //convertFileToDataURLviaFileReader(image);

  }

function send_data() {

  var email = localStorage.getItem('email');
  var password=localStorage.getItem('password');
  var title = document.getElementById('title').value;
  var image = document.getElementById('fileupload').files[0];
  var description = document.getElementById('description').value;
  var pay = document.getElementById('pay').value;
  var address = document.getElementById('address').value;

      var dataurl = "";
      //console.log(email + password + title + image + description + pay + address);

        //var width = image.clientWidth;
        //var height = image.clientHeight;
        //var end = width/height/400;

        //dataurl = imageToDataUri(image, end, 400);

        pay = pay*100;
/*
*/
        perkele = "payload="+encodeURIComponent(JSON.stringify({"email":email, "password":password, "title":title, "description": description, "pay":pay, "address":address}))+"&image="+encodeURIComponent(imagedata)+"";

      $.ajax({
            method: "POST",
            url: "https://app.taskapp.io/submitJob",
            data: perkele,//"payload="+encodeURIComponent(JSON.stringify({"email":email, "password":password, "title":title, "description": description, "pay":pay, "address":address, "image":dataurl}))+"",
            success: function(data){
              //console.log(JSON.parse(data));
              //alert(data);
              $("#loader").hide();

              slide("main_menu.html","right");
              navigator.notification.alert(
                  'Job submitted',  // message
                  null,         // callback
                  'Done',            // title
                  'Ok'                  // buttonName
              );
            }
          });

}
function preprocess(image)
{
var fileinput = image;

var max_width = 300;//fileinput.getAttribute('data-maxwidth');
var max_height = 200;//fileinput.getAttribute('data-maxheight');

//var preview = document.getElementById('preview');

//var form = document.getElementById('form');
console.log("täällä")
function processfile(file) {

    if( !( /image/i ).test( file.type ) )
        {
            alert( "File "+ file.name +" is not an image." );
            return false;
        }

    // read the files
    var reader = new FileReader();
    reader.readAsArrayBuffer(file);

    reader.onload = function (event) {
      // blob stuff
      var blob = new Blob([event.target.result]); // create blob...
      window.URL = window.URL || window.webkitURL;
      var blobURL = window.URL.createObjectURL(blob); // and get it's URL

      // helper Image object
      var image = new Image();
      image.src = blobURL;
      //preview.appendChild(image); // preview commented out, I am using the canvas instead
      image.onload = function() {
        // have to wait till it's loaded
        var resized = resizeMe(image); // send it to canvas
        var newinput = document.createElement("input");
        newinput.type = 'hidden';
        newinput.name = 'images[]';
        newinput.value = resized; // put result from canvas into new hidden input
        //form.appendChild(newinput);
      }
    };
}

function readfiles(files) {

    // remove the existing canvases and hidden inputs if user re-selects new pics
    //var existinginputs = document.getElementsByName('images[]');
    //var existingcanvases = document.getElementsByTagName('canvas');
    //while (existinginputs.length > 0) { // it's a live list so removing the first element each time
      // DOMNode.prototype.remove = function() {this.parentNode.removeChild(this);}
      //form.removeChild(existinginputs[0]);
      //preview.removeChild(existingcanvases[0]);
    //}

    for (var i = 0; i < files.length; i++) {
      processfile(files[i]); // process each file at once
    }
    fileinput.value = ""; //remove the original files from fileinput
    // TODO remove the previous hidden inputs if user selects other files
}

// this is where it starts. event triggered when user selects files

  readfiles(fileinput.files);


// === RESIZE ====

function resizeMe(img) {

  var canvas = document.createElement('canvas');

  var width = img.width;
  var height = img.height;

  // calculate the width and height, constraining the proportions
  if (width > height) {
    if (width > max_width) {
      //height *= max_width / width;
      height = Math.round(height *= max_width / width);
      width = max_width;
    }
  } else {
    if (height > max_height) {
      //width *= max_height / height;
      width = Math.round(width *= max_height / height);
      height = max_height;
    }
  }

  // resize the canvas and draw the image data into it
  canvas.width = width;
  canvas.height = height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0, width, height);

  //preview.appendChild(canvas); // do the actual resized preview

  imagedata = canvas.toDataURL("image/jpeg",0.7);
  console.log("nyt tulee");
  console.log(imagedata); // get the data from canvas as 70% JPG (can be also PNG, etc.)
  send_data();
}
}
