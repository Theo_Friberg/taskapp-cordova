
// Copyright (c) 2016 Digisnow Ltd. All rights reserved.

/*

Function to update job.

*/

function update_job(email, password, title, image, description, pay, address, callback, timestamp) {



  // Load the image into a dataurl for serialization

  var reader  = new FileReader();

  var image_data = "";

  reader.addEventListener("load", function () {

    canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");

    var img = document.createElement("img");
    img.src = reader.result

    ctx.drawImage(img, 0, 0);

    var MAX_WIDTH = 250;
    var MAX_HEIGHT = 250;
    var width = img.width;
    var height = img.height;

    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    var dataurl = canvas.toDataURL("image/jpeg", 0.7);
    request(JSON.stringify({"email":encodeURIComponent(email), "password":encodeURIComponent(password), "title":encodeURIComponent(title), "description": encodeURIComponent(description), "pay":encodeURIComponent(pay*100), "address":encodeURIComponent(address), "timestamp":timestamp})+"&image="+encodeURIComponent(dataurl), "https://app.taskapp.io/updateJob", function(http){
      var status = JSON.parse(http.responseText).status;
      if(status == "success"){
        navigator.notification.alert(
            'Job submitted.',  // message
            null,         // callback
            'Done',            // title
            'Ok'                  // buttonName
        );
        window.location = "main_menu.html"
      }else{
        navigator.notification.alert(
            'Something went wrong on our end. Try again in a little while?',  // message
            null,         // callback
            'Error',            // title
            'Ok'                  // buttonName
        );
      }
    }, function(){
      navigator.notification.alert(
          'Something went wrong on our end. Try again in a little while?',  // message
          null,         // callback
          'Error',            // title
          'Ok'                  // buttonName
      );
    });
  }, false);

  // If the image changed on the form; don't send the image to the server otherwise

  if(image){
    reader.readAsDataURL(image);
  }else{
    request(JSON.stringify({"email":encodeURIComponent(email), "password":encodeURIComponent(password), "title":encodeURIComponent(title), "description": encodeURIComponent(description), "pay":encodeURIComponent(pay), "address":encodeURIComponent(address), "timestamp":timestamp}), "https://app.taskapp.io/updateJob", function(http){
      var status = JSON.parse(http.responseText).status;
      if(status == "success"){
        navigator.notification.alert(
            'Job submitted.',  // message
            null,         // callback
            'Done',            // title
            'Ok'                  // buttonName
        );
        window.location = "main_menu.html"
      }else{
        navigator.notification.alert(
            'Something went wrong on our end. Try again in a little while?',  // message
            null,         // callback
            'Error',            // title
            'Ok'                  // buttonName
        );
      }
    }, function(){
      navigator.notification.alert(
          'Something went wrong on our end. Try again in a little while?',  // message
          null,         // callback
          'Error',            // title
          'Ok'                  // buttonName
      );
    });
  }


  return false;
}
