function onLoad(){
  document.addEventListener("deviceready", onDeviceReady, false);
}
function params_unserialize(p){
p = p.replace(/\+/, ' ');
var ret = {},
    seg = p.replace(/^\?/,'').split('&'),
    len = seg.length, i = 0, s;
for (;i<len;i++) {
    if (!seg[i]) { continue; }
    s = seg[i].split('=');
    ret[s[0]] = s[1];
}
return ret;}
function onDeviceReady() {
  var authWindow = cordova.InAppBrowser.open("https://app.taskapp.io/FBlogin", '_blank', 'location=no,toolbar=no');
  authWindow.addEventListener("loadstart", function(e){
    var done = /FBloginDone/.exec(e.url);
    if(done){
      authWindow.close()
      try{
      var code = /FBloginDone?.+$/.exec(e.url);
      var data = params_unserialize(code[0].substring(12));
      localStorage.setItem('name', decodeURIComponent(data["name"]));
      localStorage.setItem('lastname', decodeURIComponent(data["lastname"]));
      localStorage.setItem('email', decodeURIComponent(data["email"]));
      localStorage.setItem('password', decodeURIComponent(data["password"]));
      window.location = "main_menu.html";
    }catch(err){
      navigator.notification.alert(
          err.message,  // message
          null,         // callback
          'Error',            // title
          'Ok'                  // buttonName
      );
    }
    }
  }
)
}
