
// Copyright (c) 2016 Digisnow Ltd. All rights reserved.

/*

Function to log in the user. This does the physical request.

TODO: Save this information somewhere and hook this up to something meaningful.

*/

function signIn(email, password, callback, promptVerification) {
  //window.location = "main_menu.html"

  if(promptVerification){
    request(encodeURIComponent(JSON.stringify({"email":email, "password":password})), "https://app.taskapp.io/login", function(http){
      var loggedIn = JSON.parse(http.responseText).loggedIn;
      if(loggedIn){
        window.location = "main_menu.html"
      }else{
        callback("Visit the link in the email we sent you and try again.")
      }
    }, function(){
      callback("Technical difficulties.")
    });
  }else{
    request(encodeURIComponent(JSON.stringify({"email":email, "password":password})), "https://app.taskapp.io/login", function(http){
      var response = JSON.parse(http.responseText);
      var loggedIn = response.loggedIn;
      if(loggedIn){
        localStorage.setItem('name', response.name);
        localStorage.setItem('lastname', response.lastname);
        localStorage.setItem('email', response.email);
        localStorage.setItem('phonenumber', response.phonenumber);
        localStorage.setItem('password', response.password);
        window.location = "main_menu.html"
      }else{
        callback("Failed to log in. Check your email and password.")
      }
    }, function(){
      callback("Technical difficulties.")
    });
  }
  return false;
}
