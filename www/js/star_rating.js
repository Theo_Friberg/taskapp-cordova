    function populate(element, star_qty, changeCallback, stars_set){
      if(element.innerHTML.length == 1){
        stars_set = parseInt(element.innerHTML)
        element.innerHTML = ""
      }
      if(element.innerHTML == ""){
      Array.apply(null, {length: star_qty}).map(Number.call, Number).forEach(function(i){
        var star = document.createElement("span")
        star.className = "material-icons"
        star.onclick = function(){
          element.innerHTML = ""
          populate(element, star_qty, changeCallback, star_qty-i)
          changeCallback(star_qty-i, element)
        };
        if(star_qty-i-1 < stars_set){
          star.innerHTML = "star"
        }else{
          star.innerHTML = "star_border"
        }
        element.appendChild(star)
      })
      }
    }

    function inflate_all(changeCallback){
    var ratings = document.getElementsByClassName("rating");
    for(var i = 0; i < ratings.length; i++)
    {
      populate(ratings[i], 5, changeCallback, 0)
    }
  }
