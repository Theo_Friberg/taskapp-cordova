
// Copyright (c) 2016 Digisnow Ltd. All rights reserved.

/*

This function is called upon sending the account creation form. It stores the credentials to localStorage.
It also sends the request to the server and handles the response. The redirection is done by the form itself.

*/

function signUp(image, name, lastname, email, phonenumber, password, callback) {


  localStorage.setItem('name', name);
  localStorage.setItem('lastname', lastname);
  localStorage.setItem('email', email);
  localStorage.setItem('phonenumber', phonenumber);
  localStorage.setItem('password', password);








  // Load the image into a dataurl for serialization

  var reader  = new FileReader();

  var image_data = "";

  reader.addEventListener("load", function () {

    canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");

    var img = document.createElement("img");
    img.src = reader.result

    ctx.drawImage(img, 0, 0);

    var MAX_WIDTH = 250;
    var MAX_HEIGHT = 250;
    var width = img.width;
    var height = img.height;

    if (width > height) {
      if (width > MAX_WIDTH) {
        height *= MAX_WIDTH / width;
        width = MAX_WIDTH;
      }
    } else {
      if (height > MAX_HEIGHT) {
        width *= MAX_HEIGHT / height;
        height = MAX_HEIGHT;
      }
    }
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, width, height);

    var dataurl = canvas.toDataURL("image/jpeg", 0.7);
    request(encodeURIComponent(JSON.stringify({"email":email, "name":name, "lastname":lastname, "phonenumber":phonenumber, "password":password}))+"&image="+encodeURIComponent(dataurl), "https://app.taskapp.io/register",
            function(){callback("Registration successfull."); window.location = "account_verification.html"},
            function(http){callback("Something went wrong while creating your account.")}
           );
  }, false);

  // If the image changed on the form; don't send the image to the server otherwise

  if(image){
    reader.readAsDataURL(image);
  }else{
    request(encodeURIComponent(JSON.stringify({"email":email, "name":name, "lastname":lastname, "phonenumber":phonenumber, "password":password})), "https://app.taskapp.io/register",
            function(){callback("Registration successfull."); window.location = "account_verification.html"},
            function(http){callback("Something went wrong while creating your account.")}
           );
  }





















  return false;

}
