# README #

This repository hosts the Cordova frontend for the TaskApp -application. It is tested on iOS and Android.


Binary distributions are available in the Downloads section, to run the application from source do:

    git clone https://Theo_Friberg@bitbucket.org/Theo_Friberg/taskapp-cordova.git
    cd taskapp-cordova
    cordova run [platform here]


This software is copyright (c) 2016 Digisnow Ltd and released under the Apache 2 license. See here: https://www.apache.org/licenses/LICENSE-2.0